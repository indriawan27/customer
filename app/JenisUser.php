<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisUser extends Model
{
    //
    protected $table = 'jenis_user';
    protected $primaryKey = 'id_jenis_user';
    protected $fillable = ['kode_jenis_user','nama_jenis_user'];
}
