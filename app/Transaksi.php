<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    //
    protected $table = 'transaksi';
    protected $primaryKey = 'id_transaksi';
    protected $fillable = ['id_pelanggan','id_bulan','stand_awal','stand_akhir','jumlah_pemakaian','beban_pemakaian'];
}
