<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bulan;

class BulanController extends Controller
{
    //
    public function index(){
        $tampilData = Bulan::all();
        return view ('bulan.index',compact('tampilData'));
    }
    
    public function store(Request $request){
        $validasiData = $request->validate([
            'nama_bulan' => 'required|max:20'
        ]);

        $simpan = Bulan::create($validasiData);

        return redirect('/bulan')->with('sukses', 'Data Bulan berhasil ditambahkan');
    }

    public function edit($id_bulan)
    {
        $ambilData = Bulan::findOrFail($id_bulan);

        return view('bulan.edit', compact('ambilData'));
    }

    public function update(Request $request, $id_bulan){
        $validasiData = $request->validate([
            'nama_bulan' => 'required|max:20'
        ]);

        $simpanUpdate = Bulan::find($id_bulan)->update($validasiData);
        return redirect('/bulan')->with('sukses', 'Data Bulan berhasil diupdate');
    }

    public function destroy($id_bulan){
        $ambilData = Bulan::findOrFail($id_bulan);
        $hapusData = $ambilData->delete();
        return redirect('/bulan')->with('sukses', 'Data Bulan berhasil dihapus');
    }
}
