<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bulan;
use App\Pelanggan;
use App\Transaksi;
use PDF;

class TransaksiController extends Controller
{
    //
    public function index(){
        $dataPelanggan = Pelanggan::all();
        $dataBulan = Bulan::all();
        $join = Transaksi::join('pelanggan','transaksi.id_pelanggan','=','pelanggan.id_pelanggan')
                ->join('bulan','transaksi.id_bulan','=','bulan.id_bulan')
                ->select('pelanggan.*','bulan.*','transaksi.*')->get();
        
        return view('transaksi.index',compact('dataPelanggan','dataBulan','join'));
        //dd($join);
    }

    public function store(Request $request){

        $validasiData = $request->validate([
            'id_pelanggan' => 'required|numeric',
            'id_bulan' => 'required|numeric',
            'stand_awal' => 'required|numeric',
            'stand_akhir' => 'required|numeric',
            'jumlah_pemakaian' => 'required|numeric',
            'beban_pemakaian' => 'required|numeric'
        ]);

        $simpan = Transaksi::create($validasiData);
        return redirect('/transaksi')->with('sukses', 'Data Transaksi berhasil ditambahkan');
    }

    public function cetakpdf($id_transaksi){
        
        //$caridata = Transaksi::where('id_transaksi',2)->get();
        
        $join = Transaksi::join('pelanggan','transaksi.id_pelanggan','=','pelanggan.id_pelanggan')
                ->join('bulan','transaksi.id_bulan','=','bulan.id_bulan')
                ->select('pelanggan.*','bulan.*','transaksi.*')
                ->where('id_transaksi',$id_transaksi)
                ->get();
        $getNama = Transaksi::join('pelanggan','transaksi.id_pelanggan','=','pelanggan.id_pelanggan')
                    ->select('pelanggan.nama_pelanggan')
                    ->where('id_transaksi',$id_transaksi)
                    ->value('nama_pelanggan');

        $pdf = PDF::loadview('transaksi.cetakpdf',['cetakdata'=>$join]);
    	return $pdf->stream('Cetak A.n. '.$getNama.'.pdf');
        //dd($join);
    }

    public function coba(){
        $array = ['satu','dua','tiga'];
        $data = $array[0];
        dd($data);
    }
}
