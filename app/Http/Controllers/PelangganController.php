<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelanggan;
use App\JenisUser;

class PelangganController extends Controller
{
    //
    public function index(){
        $getPelanggan = Pelanggan::all();
        return view('pelanggan.index',compact('getPelanggan'));
    }

    public function store(Request $request){
        $validasiData = $request->validate([
            'nama_pelanggan' => 'required|max:20',
            'alamat' => 'required',
            'rt_pelanggan' => 'required',
            'rw_pelanggan' => 'required',
            'kode_jenis_user' => 'numeric'
        ]);

        $simpan = Pelanggan::create($validasiData);

        return redirect('/pelanggan')->with('sukses', 'Data Pelanggan berhasil ditambahkan');
    }

    public function edit($id_pelanggan)
    {
        $ambilData = Pelanggan::findOrFail($id_pelanggan);
        $getJenisUser = JenisUser::select('kode_jenis_user','nama_jenis_user')->get();
        return view('pelanggan.edit', compact('ambilData','getJenisUser'));
    }

    public function update(Request $request, $id_pelanggan){
        $validasiData = $request->validate([
            'nama_pelanggan' => 'required|max:20',
            'alamat' => 'required',
            'rt_pelanggan' => 'required',
            'rw_pelanggan' => 'required',
            'kode_jenis_user' => 'numeric'
        ]);

        $update = Pelanggan::find($id_pelanggan)->update($validasiData);

        return redirect('/pelanggan')->with('sukses', 'Data Pelanggan berhasil diedit');
    }
}
