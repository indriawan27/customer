<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisUser;

class JenisUserController extends Controller
{
    //
    public function index(){
        $getData = JenisUser::all();
        return view('jenisuser.index',compact('getData'));
    }

    public function store(Request $request){
        $validasiData = $request->validate([
            'kode_jenis_user' => 'required|max:1|numeric',
            'nama_jenis_user' => 'required'
        ]);

        
        $simpan = JenisUser::create($validasiData);
        return redirect('/jenisuser')->with('sukses', 'Data Baru berhasil ditambahkan');
    }
}
