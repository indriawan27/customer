-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 02 Des 2021 pada 08.27
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `customer`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bulan`
--

CREATE TABLE `bulan` (
  `id_bulan` int(15) NOT NULL,
  `nama_bulan` varchar(15) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `bulan`
--

INSERT INTO `bulan` (`id_bulan`, `nama_bulan`, `created_at`, `updated_at`) VALUES
(1, 'Januari', '2021-11-22 19:19:27', '2021-11-26 23:52:56'),
(2, 'Februari', '2021-11-26 23:53:14', '2021-11-26 23:53:14'),
(3, 'Maret', '2021-11-26 23:53:19', '2021-11-26 23:53:19'),
(4, 'April', '2021-11-26 23:53:23', '2021-11-26 23:53:23'),
(5, 'Mei', '2021-11-26 23:53:27', '2021-11-26 23:53:27'),
(6, 'Juni', '2021-11-26 23:53:31', '2021-11-26 23:53:31'),
(7, 'Juli', '2021-11-26 23:53:36', '2021-11-26 23:53:36'),
(8, 'Agustus', '2021-11-26 23:53:43', '2021-11-26 23:53:43'),
(9, 'September', '2021-11-26 23:53:56', '2021-11-26 23:53:56'),
(10, 'Oktober', '2021-11-26 23:54:03', '2021-11-26 23:54:03'),
(11, 'November', '2021-11-26 23:54:08', '2021-11-26 23:54:08'),
(12, 'Desember', '2021-11-26 23:54:14', '2021-11-26 23:54:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(15) NOT NULL,
  `nama_pelanggan` varchar(200) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `rt_pelanggan` varchar(2) NOT NULL,
  `rw_pelanggan` varchar(2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pelanggan`
--

INSERT INTO `pelanggan` (`id_pelanggan`, `nama_pelanggan`, `alamat`, `rt_pelanggan`, `rw_pelanggan`, `created_at`, `updated_at`) VALUES
(1, 'Paijo', 'Solo', '02', '01', '2021-11-27 00:18:03', '2021-11-27 00:18:03'),
(2, 'Anggar', 'Sukoharjo', '09', '10', '2021-11-27 00:36:11', '2021-11-27 01:41:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(15) UNSIGNED NOT NULL,
  `id_pelanggan` int(15) NOT NULL,
  `id_bulan` int(15) NOT NULL,
  `stand_awal` varchar(15) NOT NULL,
  `stand_akhir` varchar(15) NOT NULL,
  `jumlah_pemakaian` varchar(15) NOT NULL,
  `beban_pemakaian` varchar(15) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_pelanggan`, `id_bulan`, `stand_awal`, `stand_akhir`, `jumlah_pemakaian`, `beban_pemakaian`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '346', '356', '10', '5000', '2021-12-01 00:20:30', '2021-12-01 00:20:30'),
(2, 2, 7, '346', '356', '98', '5000', '2021-12-01 02:28:06', '2021-12-01 02:28:06');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `bulan`
--
ALTER TABLE `bulan`
  ADD PRIMARY KEY (`id_bulan`);

--
-- Indeks untuk tabel `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_pelanggan` (`id_pelanggan`),
  ADD KEY `id_bulan` (`id_bulan`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `bulan`
--
ALTER TABLE `bulan`
  MODIFY `id_bulan` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id_pelanggan` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(15) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_pelanggan`) REFERENCES `pelanggan` (`id_pelanggan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`id_bulan`) REFERENCES `bulan` (`id_bulan`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
