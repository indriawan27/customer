<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/edit',function(){
    return view('bulan.edit');
});

Route::get('/bulan','BulanController@index');
Route::get('/bulan/{id_bulan}/edit/','BulanController@edit')->name('bulan.edit');
Route::put('/bulan/update/{id_bulan}', 'BulanController@update')->name('bulan.update');
Route::get('/karyawan/{nik_karyawan}/show','KaryawanController@show');
Route::post('/bulan/store','BulanController@store')->name('bulan.store');
Route::get('/karyawan/delete/{nik_karyawan}','KaryawanController@destroy')->name('bulan.destroy');

Route::get('/pelanggan','PelangganController@index');
Route::post('/pelanggan/store','PelangganController@store')->name('pelanggan.store');
Route::get('/pelanggan/{id_pelanggan}/edit/','PelangganController@edit')->name('pelanggan.edit');
Route::put('/pelanggan/update/{id_pelanggan}', 'PelangganController@update')->name('pelanggan.update');

Route::get('/transaksi','TransaksiController@index');
Route::post('/transaksi/store','TransaksiController@store')->name('transaksi.store');
Route::get('/transaksi/cetak/{id_transaksi}', 'TransaksiController@cetakpdf')->name('transaksi.cetak');
Route::get('/transaksi/coba','TransaksiController@coba');

Route::get('/jenisuser','JenisUserController@index');
Route::post('/jenisuser/store','JenisUserController@store')->name('jenisuser.store');