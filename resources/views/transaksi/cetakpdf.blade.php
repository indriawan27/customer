<html>
    <head>
         @foreach ($cetakdata as $item)
         <title>Tagihan Atas Nama {{ $item->nama_pelanggan }}</title>
         
    </head>
    <body>
            <table style="border-collapse: collapse; border-style: solid">
                <tr>
                    <td colspan="2"><center>TANDA PEMBAYARAN REKENING AIR</center></td>
                </tr>
                <tr>
                    <td colspan="2"><center>BP-SPAM "RAHAYU 2" DESA BENTAK</center></td>
                </tr>
                <tr>
                    <td>BULAN</td>
                    <td>: {{ $item->nama_bulan }}</td>
                </tr>
                <tr>
                    <td>NO</td>
                    <td>: {{ $item->id_pelanggan }}</td>
                </tr>
                <tr>
                    <td>NAMA</td>
                    <td>: {{ $item->nama_pelanggan }}</td>
                </tr>
                <tr>
                    <td>RT</td>
                    <td>: {{ $item->rt_pelanggan }} </td>
                </tr>
                <tr>
                    <td>RW</td>
                    <td>: {{ $item->rw_pelanggan }}</td>
                </tr>
                <tr>
                    <td>STAND AWAL</td>
                    <td>: {{ $item->stand_awal }}</td>
                </tr>
                <tr>
                    <td>STAND AKHIR</td>
                    <td>: {{ $item->stand_akhir }}</td>
                </tr>
                <tr>
                    <td>JUMLAH PEMAKAIAN</td>
                    <td>: {{ $item->jumlah_pemakaian }}</td>
                </tr>
                <tr>
                    <td>TARIF PER METER</td>
                    <td>: Rp 1,500.00</td>
                </tr>
                <tr>
                    <td>BEBAN</td>
                    <td>: Rp 5,000.00</td>
                </tr>
                <tr>
                    <td>BIAYA PEMAKAIAN</td>
                    <td>: {{ $item->jumlah_pemakaian*1500 }}</td>
                </tr>
                <tr>
                    <td>TOTAL</td>
                    <td>: {{ ($item->jumlah_pemakaian*1500)+5000 }}</td>
                </tr>
            </table>
        @endforeach
    </body>
</html>