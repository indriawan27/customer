@extends('template')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    {{-- <h1>Blank Page</h1> --}}
                </div>
                <div class="col-sm-6">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                    @endif
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data Transaksi</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                  Tambah Data
                </button></h3>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 10px">ID Transaksi</th>
                                    <th>Bulan</th>
                                    <th>Nama Pelanggan</th>
                                    <th>Alamat</th>
                                    <th>RT</th>
                                    <th>RW</th>
                                    <th>Stand Awal</th>
                                    <th>Stand Akhir</th>
                                    <th>Jumlah Pemakaian</th>
                                    <th>TOTAL</th>
                                    <th>Action</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($join as $item)
                                <tr>
                                    <td>{{ $item->id_transaksi }}</td>
                                    <td>{{ $item->nama_bulan }}</td>
                                    <td>{{ $item->nama_pelanggan }}</td>
                                    <td>{{ $item->alamat }}</td>
                                    <td>{{ $item->rt_pelanggan }}</td>
                                    <td>{{ $item->rw_pelanggan }}</td>
                                    <td>{{ $item->stand_awal }}</td>
                                    <td>{{ $item->stand_akhir }}</td>
                                    <td>{{ 1500*$item->jumlah_pemakaian }}</td>
                                    <td>{{ (1500*$item->jumlah_pemakaian)+$item->beban_pemakaian }}</td>
                                    
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('bulan.edit', $item->id_bulan)}}" class="btn btn-warning">Edit</a>
                                        </div>
                                        <div class="btn-group">
                                            <form action="{{ route('bulan.destroy', $item->id_bulan) }}" method="POST">
                                                @csrf
                                                
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </form>
                                        </div>
                                            {{-- <a href="{{ route('bulan.edit', $item->id_bulan)}}" class="btn btn-success">Detail</a> --}}
                                        <div class="btn-group">
                                            <a href="{{ route('transaksi.cetak', $item->id_transaksi )}}" class="btn btn-success" target="_blank">Cetak</a>
                                        </div>
                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                Footer
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Data Transaksi</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form method="post" action="{{ route('transaksi.store') }}">
                @csrf
            <div class="modal-body">
              <div class="form-group">
                    <label for="nama_pelanggan">Nama Pelanggan</label>
                    <select class="form-control" name="id_pelanggan" id="nama_pelanggan">
                        @foreach ($dataPelanggan as $item)
                            <option value="{{ $item->id_pelanggan }}">{{ $item->nama_pelanggan }}</option>
                        @endforeach
                          
                    </select>
                </div>
                <div class="form-group">
                    <label for="bulan">Bulan</label>
                    <select class="form-control" name="id_bulan" id="bulan">
                          @foreach ($dataBulan as $item)
                            <option value="{{ $item->id_bulan }}">{{ $item->nama_bulan }}</option>
                        @endforeach
                    </select>
                </div>
                    <div class="form-group">
                        <label>Stand Awal</label>
                        <input type="text" name="stand_awal" class="form-control" placeholder="Enter ...">
                    </div>
                    <div class="form-group">
                        <label>Stand Akhir</label>
                        <input type="text" name="stand_akhir" class="form-control" placeholder="Enter ...">
                    </div>
                    <div class="form-group">
                        <label>Jumlah Pemakaian</label>
                        <input type="text" name="jumlah_pemakaian" class="form-control" placeholder="Enter ...">
                    </div>
                    <div class="form-group">
                        <label>Beban Pemakaian</label>
                        <input type="text" name="beban_pemakaian" class="form-control" placeholder="Enter ...">
                    </div>
                
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
        </form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
@endsection