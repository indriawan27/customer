@extends('template');

@section('content')
    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    {{-- <h1>Blank Page</h1> --}}
                </div>
                <div class="col-sm-6">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                    @endif
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data Pelanggan</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                  Tambah Data
                </button></h3>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 10px">ID Pelanggan</th>
                                    <th>Nama Pelanggan</th>
                                    <th>Alamat Bulan</th>
                                    <th>RT</th>
                                    <th>RW</th>
                                    <th>Jenis User</th>
                                    <th>Action</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($getPelanggan as $item)
                                <tr>
                                    <td>{{ $item->id_pelanggan }}</td>
                                    <td>{{ $item->nama_pelanggan }}</td>
                                    <td>{{ $item->alamat }}</td>
                                    <td>{{ $item->rt_pelanggan }}</td>
                                    <td>{{ $item->rw_pelanggan }}</td>
                                    <td>@php
                                        if ($item->kode_jenis_user == 0) {
                                            echo 'User';
                                        }
                                        else {
                                            echo 'Administrator';
                                        }
                                    @endphp</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('pelanggan.edit', $item->id_pelanggan)}}" class="btn btn-warning">Edit</a>
                                            <a href="{{ route('bulan.edit', $item->id_pelanggan)}}" class="btn btn-danger">Hapus</a>
                                            <a href="{{ route('bulan.edit', $item->id_pelanggan)}}" class="btn btn-success">Detail</a>
                                        </div>
                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                Footer
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Data Pelanggan</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form method="post" action="{{ route('pelanggan.store') }}">
                @csrf
            <div class="modal-body">
              <div class="form-group">
                    <label for="nama_pelanggan">Nama Pelanggan</label>
                    <input type="text" name="nama_pelanggan" class="form-control" id="nama_pelanggan" name="nama_pelanggan" placeholder="Nama Pelanggan">
                  </div>
                 <div class="form-group">
                    <label for="alamat">Alamat Pelanggan</label>
                    <input type="text" name="alamat" class="form-control" id="alamat" name="alamat" placeholder="Alamat">
                  </div>
                   <div class="form-group">
                    <label for="rt_pelanggan">RT Pelanggan</label>
                    <input type="text" name="rt_pelanggan" class="form-control" id="rt_pelanggan" name="rt_pelanggan" placeholder="RT">
                  </div>
                   <div class="form-group">
                    <label for="rw_pelanggan">RW Pelanggan</label>
                    <input type="text" name="rw_pelanggan" class="form-control" id="rw_pelanggan" name="rw_pelanggan" placeholder="RW">
                  </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
        </form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
@endsection