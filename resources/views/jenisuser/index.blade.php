@extends('template')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    {{-- <h1>Blank Page</h1> --}}
                </div>
                <div class="col-sm-6">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                    @endif
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Jenis User</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#modal-default">
                                Tambah Data
                            </button></h3>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 10px">ID Jenis User</th>
                                    <th>Kode Jenis User</th>
                                    <th>Nama Jenis User</th>
                                    <th>Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($getData as $item)
                                <tr>
                                    <td>{{ $item->id_jenis_user }}</td>
                                    <td>{{ $item->kode_jenis_user }}</td>
                                    <td>{{ $item->nama_jenis_user }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('bulan.edit', $item->id_jenis_user)}}"
                                                class="btn btn-warning">Edit</a>
                                            <form action="{{ route('bulan.destroy', $item->id_jenis_user) }}" method="POST">
                                                @csrf

                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </form>
                                        </div>
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                Footer
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data Jenis User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form method="post" action="{{ route('jenisuser.store') }}">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="kode_jenis_user">Kode Jenis User</label>
                        <input type="text" name="kode_jenis_user" class="form-control" id="kode_jenis_user" name="kode_jenis_user"
                            placeholder="Kode Jenis User">
                    </div>
                    <div class="form-group">
                        <label for="nama_jenis_user">Nama Jenis User</label>
                        <input type="text" name="nama_jenis_user" class="form-control" id="nama_jenis_user" name="kode_jenis_user"
                            placeholder="Nama Jenis User">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
        </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection