@extends('template')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    {{-- <h1>Blank Page</h1> --}}
                </div>
                <div class="col-sm-6">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                    @endif
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data Bulan</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                  Tambah Data
                </button></h3>
                    </div>
                    <div class="card-body p-0">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 10px">ID Bulan</th>
                                    <th>Nama Bulan</th>
                                    <th>Action</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($tampilData as $item)
                                <tr>
                                    <td>{{ $item->id_bulan }}</td>
                                    <td>{{ $item->nama_bulan }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('bulan.edit', $item->id_bulan)}}" class="btn btn-warning">Edit</a>
                                            <form action="{{ route('bulan.destroy', $item->id_bulan) }}" method="POST">
                                                @csrf
                                                
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </form>
                                            {{-- <a href="{{ route('bulan.edit', $item->id_bulan)}}" class="btn btn-success">Detail</a> --}}
                                        </div>
                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                Footer
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Data Bulan</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <form method="post" action="{{ route('bulan.store') }}">
                @csrf
            <div class="modal-body">
              <div class="form-group">
                    <label for="nama_bulan">Nama Bulan</label>
                    <input type="text" name="nama_bulan" class="form-control" id="nama_bulan" name="nama_bulan" placeholder="Nama Bulan">
                  </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
        </form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
@endsection
