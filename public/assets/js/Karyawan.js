    var SITEURL = '{{ URL::to("") }}';
    
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#tabel-karyawan').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: SITEURL + "/karyawan",
                type: 'GET',
            },
            columns: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'nik_karyawan',
                    name: 'nik_karyawan',
                    
                },
                {
                    data: 'image',
                    name: 'image',
                    orderable: false
                },
                {
                    data: 'nama_karyawan',
                    name: 'nama_karyawan'
                },
                {
                    data: 'alamat_karyawan',
                    name: 'alamat_karyawan'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                },
            ],
            order: [
                [0, 'desc']
            ]
        });
    /*  When user click add user button */
    $('#create-karyawan').click(function () {
        $('#btn-save').val("create-karyawan");
        $('#nikkaryawan').val('');
        $('#formKaryawan').trigger("reset");
        $('#judulModal').html("Tambah data karyawan");
        $('#modalKaryawan').modal('show');
        $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
    });
    /* When click edit user */
    $('body').on('click', '.edit-karyawan', function () {
        var nikkaryawan = $(this).attr('data-id-karyawan');
        $('#modal-preview').attr('src', "https://via.placeholder.com/150");
        $.get('karyawan/' + nikkaryawan + '/edit', function (data) {
            $('#judulModal').html("Edit Karyawan ");
            $("#formKaryawan").find("input[name=nikkaryawan]").val(data.nik_karyawan);          
            $("#formKaryawan").find("input[name=nik_karyawan]").val(data.nik_karyawan);
            $("#formKaryawan").find("input[name=nibm_karyawan]").val(data.nibm_karyawan);
            $("#formKaryawan").find("input[name=nama_karyawan]").val(data.nama_karyawan);
            $("#formKaryawan").find("input[name=ktp_karyawan]").val(data.ktp_karyawan);
            $("#formKaryawan").find("input[name=alamat_karyawan]").val(data.alamat_karyawan);
            $("#formKaryawan").find("input[name=tempat_lahir]").val(data.tempat_lahir);
            $("#formKaryawan").find("input[name=tanggal_lahir]").val(data.tanggal_lahir);
            $("#formKaryawan").find("input[name=no_telp]").val(data.no_telp);
            $("#formKaryawan").find("input[name=no_hp]").val(data.no_hp);
            $("#formKaryawan").find("input[name=status_pernikahan]").val(data.status_pernikahan);
            $("#formKaryawan").find("input[name=jml_anak]").val(data.jml_anak);
            $("#formKaryawan").find("input[name=nama_pasangan]").val(data.nama_pasangan);
            $("#formKaryawan").find("input[name=ayah_kandung]").val(data.ayah_kandung);
            $("#formKaryawan").find("input[name=ibu_kandung]").val(data.ibu_kandung);
            $("#formKaryawan").find("input[name=status_karyawan]").val(data.status_karyawan);
            $('#modal-preview').attr('alt', 'No image available');
            $('#btn-save').val("edit-karyawan");
            $('#modalKaryawan').modal('show');
            if (data.foto_karyawan) {
                $('#modal-preview').attr('src', SITEURL + '/assets/image/karyawan/' + data.foto_karyawan);
                $('#hidden_image').attr('src', SITEURL + '/assets/image/karyawan/' + data.foto_karyawan);
            }
        })
    });
    $('body').on('click', '#hapus-karyawan', function () {
        var nikkaryawan = $(this).attr("data-id-karyawan");
        if (confirm("Anda yakin akan menghapus data?")) {
            $.ajax({
                type: "get",
                url: SITEURL + "/karyawan/delete/" + nikkaryawan,
                success: function (data) {
                    var oTable = $('#tabel-karyawan').dataTable();
                    oTable.fnDraw(false);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    });
});
$('body').on('submit', '#formKaryawan', function (e) {
    e.preventDefault();
    var actionType = $('#btn-save').val();
    $('#btn-save').html('Sending..');
    var formData = new FormData(this);
    $.ajax({
        type: 'POST',
        url: SITEURL + "/karyawan/store",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: (data) => {
            $('#formKaryawan').trigger("reset");
            $('#modalKaryawan').modal('hide');
            $('#btn-save').html('Simpan Perubahan');
            var oTable = $('#tabel-karyawan').dataTable();
            oTable.fnDraw(false);
        },
        error: function (data) {
            console.log('Error:', data);
            $('#btn-save').html('Save Changes');
        }
    });
});

function readURL(input, nikkaryawan) {
    nikkaryawan = nikkaryawan || '#modal-preview';
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(nikkaryawan).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        $('#modal-preview').removeClass('hidden');
        $('#start').hide();
    }
}


    $('#tanggal_lahir').datetimepicker( {
            format: 'YYYY-MM-DD',
            autoclose: true,
        }

    );

    $(function () {
            bsCustomFileInput.init();
        }

    );
